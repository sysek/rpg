import sys
from random import randint
from time import sleep
import json


def dice():
    stat = []
    for i in range(4):
        stat.append(randint(1, 6))

    stat.sort(reverse=True)
    return stat[0] + stat[1] + stat[2]


def generate_ab():
    abilities = {
        'Strength': 0,
        'Dexterity': 0,
        'Constitution': 0,
        'Intelligence': 0,
        'Wisdom': 0,
        'Charisma': 0
    }

    for k, v in abilities.items():
        abilities[k] = dice()

    for k, v in abilities.items():
        print("{}: \t{}".format(k, v))

    return abilities


def create_user():
    races = ['dwarf', 'elf', 'halfling', 'human']
    name = input("Name: ")
    for race in races:
        print(race.capitalize())

    race = input("Race: ")

    print("--- GENERATE STATS ---")
    print("*------- {} ---------*".format(name.capitalize()))

    abi = generate_ab()
    odp = input("Czy chcesz zapisać? y/n ")
    if odp is 'y':
        with open('char.json', 'w') as f:
            json.dump(abi, f, indent=4)

        print("Postać została zapisana")
    else:
        main()


def hello_screen():
    message = """
       _____   _____ ____   _____ 
      |  __ \ / ____/ __ \ / ____|
      | |  | | |   | |  | | (___  
      | |  | | |   | |  | |\___ \ 
      | |__| | |___| |__| |____) |
      |_____/ \_____\____/|_____/ 
    """
    print(message)
    print("-- [0]. Create User --")
    print("-- [1]. INFO --")
    print("-- [E]. Exit")
    ans = input("> ")
    return ans


def info():
    print("Author: sysek")
    print("Wersja: 0.0.2")
    print("(C) PCCC")


def main():
    while True:
        menu = hello_screen()
        if menu is '0':
            create_user()
        elif menu is '1':
            info()
        elif menu is 'E':
            sys.exit(0)


if __name__ == '__main__':
    main()
